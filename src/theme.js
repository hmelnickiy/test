import { theme } from "@chakra-ui/core";
export default  {
  ...theme,
  colors: {
    ...theme.colors,
    // ['yellow.400']: '#EC9C00',
    blue: {
      
      100: "#153e75",
      200: "#2a69ac",
    },
    gray: {
        100: '#cccccc'
    },
    white: {
        100: '#fff'
    }
      
  },
  breakpoints: ["768px", "1200px"],
  fonts: {
    heading: 'gilroy, "Avenir Next", sans-serif',
    body: 'gilroy, "Avenir Next", system-ui, sans-serif',
    mono: "Menlo, monospace",
  },
  fontSizes: {
    xs: "0.75rem",
    sm: "0.875rem",
    md: "1rem",
    lg: "1.125rem",
    xl: "1.25rem",
    "2xl": "1.5rem",
    "3xl": "1.875rem",
    "4xl": "2.25rem",
    "5xl": "3rem",
    "6xl": "4rem",
  },
  shadows : {
  sm: "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
  md: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
  lg: "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
  xl:
    "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
  "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
  outline: "0 0 0 3px rgba(66, 153, 225, 0.6)",
  inner: "inset 0 2px 4px 0 rgba(0,0,0,0.06)",
  none: "none",
},
  radii: {
    ...theme.radii,
    sm: '6px',
    md: '6px',
  },
  borders: {
        none: 0,
        "1px": "1px solid",
        "2px": "2px solid",
        "4px": "24px solid",
    }
};
